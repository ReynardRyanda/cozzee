from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import inputKomen, deleteKomen
from .forms import formKomen
from .models import simpanKomen


class AppCommentTest(TestCase):
    def test_templateGaAda(self):
        response = self.client.get('/gajelas/')
        self.assertEqual(response.status_code,404)

    def test_formCommentExist(self):
        response = self.client.get('/indexComment/1/')
        self.assertEqual(response.status_code,200)
    
    # def test_hasilCommentExist(self):
        
    #     simpanKomen.objects.create(fieldJudulKomen = "Kopinya enak bro",
    #     fieldIsiKomen = "rasanya manis, tempatnya keren",
    #     fieldRatingR = 3,
    #     fieldRatingS = 4,
    #     fieldRatingP = 5,
    #     id_toko = 1)

    #     response = self.client.get('/toko/1/')
    #     self.assertEqual(response.status_code,200)
        
    # def test_using_formComment(self):
    #     found = resolve('/indexComment/<int:id>/')
    #     self.assertEqual(found.func, inputKomen)
    
    # def test_uses_formComment_template(self):
    #     response = self.client.get('/indexComment/<int:id>/')
    #     self.assertTemplateUsed(response, 'input.html')
    
    def test_comment_form(self):
        simpanKomen.objects.create(fieldJudulKomen = "Kopinya enak bro",
        fieldIsiKomen = "rasanya manis, tempatnya keren",
        fieldRatingR = 3,
        fieldRatingS = 4,
        fieldRatingP = 5,
        id_toko = 1)

        hitungJumlah = simpanKomen.objects.all().count()
        self.assertEqual(hitungJumlah,1)
    
    def test_delete_comment(self):
        simpanKomen.objects.create(fieldJudulKomen = "Kopinya enak bro",
        fieldIsiKomen = "rasanya manis, tempatnya keren",
        fieldRatingR = 3,
        fieldRatingS = 4,
        fieldRatingP = 5,
        id_toko = 1)

        allComment = simpanKomen.objects.all()

        response = self.client.get('/delete/')
        hitungJumlah = simpanKomen.objects.all().count()
        self.assertEqual(response.status_code,200)
        self.assertEqual(hitungJumlah,0)
    
    # def test_inputhasilForm(self):
    #     simpanKomen.objects.create(fieldJudulKomen = "Kopinya enak bro",
    #     fieldIsiKomen = "rasanya manis, tempatnya keren",
    #     fieldRatingR = 3,
    #     fieldRatingS = 4,
    #     fieldRatingP = 5,
    #     id_toko = 1)
    #     response = self.client.get('/input/1/')
        
    #     hitungJumlah = simpanKomen.objects.all().count()
    #     self.assertEqual(hitungJumlah,1)

    # def test_rata_tiga(self):
    #     simpanKomen.objects.create(fieldJudulKomen = "Kopinya enak bro",
    #     fieldIsiKomen = "rasanya manis, tempatnya keren",
    #     fieldRatingR = 3,
    #     fieldRatingS = 4,
    #     fieldRatingP = 5,
    #     id_toko = 1)




    