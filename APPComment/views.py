from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import formKomen
from .models import simpanKomen
from landingpage.views import toko

def index(request, id):
    formulir = formKomen()
    argument = {
        'generated_html' : formulir,
        'id' : id
    }
    return render(request, 'input.html', argument)

def inputKomen(request, id):
    a = request.POST['fieldJudulKomen']
    b = request.POST['fieldIsiKomen']
    c = request.POST['fieldRatingR']
    d = request.POST['fieldRatingS']
    e = request.POST['fieldRatingP']
    f = id

    Komen = simpanKomen(
        fieldJudulKomen = a,
        fieldIsiKomen = b,
        fieldRatingR = c,
        fieldRatingS = d,
        fieldRatingP = e,
        id_toko = f
    )
    Komen.save()
    countRasa = 0
    countSuasana = 0
    countPelayanan = 0
    komen = simpanKomen.objects.all()
    for x in komen:
        countRasa += x.fieldRatingR
        countSuasana += x.fieldRatingS
        countPelayanan += x.fieldRatingP
    banyakIsi = simpanKomen.objects.all().count()
    rataRataTotal = rataRata(countRasa, countSuasana, countPelayanan)
    countRasa = round(countRasa/banyakIsi, 1)
    countSuasana = round(countSuasana/banyakIsi, 1)
    countPelayanan = round(countPelayanan/banyakIsi, 1)
    rataRataTotal = round(rataRataTotal/banyakIsi, 1)
    argumen = {
        'countR' : 2*countRasa,
        'countS' : 2*countSuasana,
        'countP' : 2*countPelayanan,
        'rataTotal' : 2*rataRataTotal
    }
    return toko(request,id, argumen)

def deleteKomen(request):
    all = simpanKomen.objects.all()
    for x in all:
        x.delete()
    countRasa = 0
    countSuasana = 0
    countPelayanan = 0
    komen = simpanKomen.objects.all()
    for x in komen:
        countRasa += x.fieldRatingR
        countSuasana += x.fieldRatingS
        countPelayanan += x.fieldRatingP
    rataRataTotal = rataRata(countRasa, countSuasana, countPelayanan)
    argumen = {
        'countR' : countRasa,
        'countS' : countSuasana,
        'countP' : countPelayanan,
        'rataTotal' : rataRataTotal,
        'komen' : komen
    }
    return render(request, 'hasil.html', argumen)

def rataRata(ratingR, ratingS, ratingP):
    return (ratingP+ratingR+ratingS)/3

# def tigaRata(request):
#     countRasa = 0
#     countSuasana = 0
#     countPelayanan = 0
#     komen = simpanKomen.objects.all()
#     for x in komen:
#         countRasa += x.fieldRatingR
#         countSuasana += x.fieldRatingS
#         countPelayanan += x.fieldRatingP
#     banyakIsi = simpanKomen.objects.all().count()
#     rataRataTotal = rataRata(countRasa, countSuasana, countPelayanan)
#     countRasa = round(countRasa/banyakIsi, 2)
#     countSuasana = round(countSuasana/banyakIsi, 2)
#     countPelayanan = round(countPelayanan/banyakIsi, 2)
#     rataRataTotal = round(rataRataTotal/banyakIsi, 2)
#     argumen = {
#         'countR' : countRasa,
#         'countS' : countSuasana,
#         'countP' : countPelayanan,
#         'rataTotal' : rataRataTotal,
#         'komen' : komen
#     }
#     return render(request, 'hasil.html', argumen)


    
