[![pipeline status](https://gitlab.com/ReynardRyanda/cozzee/badges/master/pipeline.svg)](https://gitlab.com/ReynardRyanda/cozzee/commits/master)

Reynard Adha Ryanda  
Muhammad Erlangga  
Ayila Adzkiya Sucahyo  
Khansa Khairunisa Putri


Link herokuapp : cozzee.herokuapp.com

<bold>Cerita aplikasi kami<bold>
----
Website Cozzee merupakan aplikasi yang berisi tentang informasi kedai - kedai kopi dimana informasi tersebut terdiri dari menu yang ada di kedai, lokasi, jam buka, fasilitas, serta kisaran harga dari kedai tersebut. Selain itu website cozzee juga menampilkan rating dari kedai tersebut berdasarkan review dari pegunjung/user dimana pengunjung/user dapat mengisi review dengan mengisi form yang ada pada website. User/pengunjung juga dapat menambahkan kedai dan menu dengan mengisi form yang ada di navigasi bar selain itu untuk meningkatkan pelayanan dan kualitas website, ditambahkan pula page Kritik dan Saran yang dapat diisi oleh user/pengunjung. Website ini dapat membantu para pelajar, karyawan kantor, freelancer ataupun ibu - ibu yang membutuhkan informasi kedai kopi yang sesuai untuk kebutuhan masing-masing (belajar, meeting, dll).

<bold>Daftar fitur yang akan diimplementasikan<bold>
----
1. Form kritik dan saran
2. Form tambah kedai
3. Form komentar
4. Form tambah menu
