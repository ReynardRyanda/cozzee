from django.apps import AppConfig


class CozzeeConfig(AppConfig):
    name = 'cozzee'
