from django.forms import ModelForm
from .models import TambahToko

class TambahTokoForm(ModelForm):
    class Meta:
      model = TambahToko
      fields = '__all__'
