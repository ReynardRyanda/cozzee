from django.shortcuts import render, redirect
from APPComment.models import simpanKomen
from .models import TambahToko
from .forms import TambahTokoForm

# Create your views here.

def index(request):
    context = {
        'kedai' : TambahToko.objects.all()
    }
    return render(request,'index.html', context)  

def toko(request, id, optional = None):
    kedai = TambahToko.objects.get(id = id)
    try:
        komen = simpanKomen.objects.filter(id_toko = id)
    except:
        komen = None

    if komen == None or optional == None:
        countRasa = 0
        countSuasana = 0
        countPelayanan = 0
        for x in komen:
            countRasa += x.fieldRatingR
            countSuasana += x.fieldRatingS
            countPelayanan += x.fieldRatingP
        banyakIsi = simpanKomen.objects.all().count()
        if banyakIsi != 0:
            rataRataTotal = (countRasa+ countSuasana+ countPelayanan)/3
            countRasa = round(countRasa/banyakIsi, 1)
            countSuasana = round(countSuasana/banyakIsi, 1)
            countPelayanan = round(countPelayanan/banyakIsi, 1)
            rataRataTotal = round(rataRataTotal/banyakIsi, 1)
            optional = {
                'countR' : 2*countRasa,
                'countS' : 2*countSuasana,
                'countP' : 2*countPelayanan,
                'rataTotal' : 2*rataRataTotal
            }
    
    return render(request,'toko.html', {'kedai' :kedai, 'komen' : komen, 'nilai': optional})


def tambahToko(request):
    form = TambahTokoForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect('/')

    return render(request, 'tambahToko.html', {
    'form' : form
  })