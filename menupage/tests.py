from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpResponse
from .views import TambahMenu, TampilanMenu
from .forms import MenuForm
from .models import buatMenu

# Create your tests here.
class menuTest(TestCase):
    # def test_menu_is_exist(self):
    #     response = Client().get('/menu-page/0/')
    #     self.assertEqual(response.status_code,200)

    def test_menu_not_exist(self):
        response = Client().get('/buatMenu/')
        self.assertEqual(response.status_code, 404)
    
    def test_using_MenuForm(self):
        buatMenu.objects.create(Nama_Menu = "Kopi Susu",
        Harga_Menu = "Rp20.000", Masukkan_Foto_Menu= "https://assets-pergikuliner.com/Shm3HsZZN8e1BLORfHq3n2jnTP4=/385x290/smart/https://assets-pergikuliner.com/uploads/image/picture/1151477/picture-1542254875.jpg",
        id_toko=1)
        found = resolve('/tambah-menu/1/')
        self.assertEqual(found.func, TambahMenu)

    def test_using_MenuForm2(self):
        buatMenu.objects.create(Nama_Menu = "Kopi Susu",
        Harga_Menu = "Rp20.000", Masukkan_Foto_Menu= "https://assets-pergikuliner.com/Shm3HsZZN8e1BLORfHq3n2jnTP4=/385x290/smart/https://assets-pergikuliner.com/uploads/image/picture/1151477/picture-1542254875.jpg,",
        id_toko=1)
        found = resolve('/menu-page/1/')
        self.assertEqual(found.func, TampilanMenu)
    
    # def test_uses_form_template(self):
    #     response = self.client.get('/tambah-menu-page/')
    #     self.assertTemplateUsed(response, 'TambahMenuPage.html')

    # def test_uses_form_template2(self):
    #     response = self.client.get('/menu-page/')
    #     self.assertTemplateUsed(response, 'TampilanMenuPage.html')
    
    # def test_feedback_form(self):
    #     buatMenu.objects.create(Nama_Menu = "Kopi Susu",
    #     Harga_Menu = "Rp20.000", Masukkan_Foto_Menu= "https://assets-pergikuliner.com/Shm3HsZZN8e1BLORfHq3n2jnTP4=/385x290/smart/https://assets-pergikuliner.com/uploads/image/picture/1151477/picture-1542254875.jpg")

    #     hitungJumlah = buatMenu.objects.all().count()
    #     self.assertEqual(hitungJumlah,1)
    
    # def test_inputhasilForm(self):
    #     response = self.client.post('/menu-page/', {
    #         'Nama_Menu': "Kopi Susu",
    #         'Harga_Menu': "Rp20.000",
    #         'Masukkan_Foto_Menu' : "https://assets-pergikuliner.com/Shm3HsZZN8e1BLORfHq3n2jnTP4=/385x290/smart/https://assets-pergikuliner.com/uploads/image/picture/1151477/picture-1542254875.jpg"
    #     })
        
    #     hitungJumlah = buatMenu.objects.all().count()
    #     self.assertEqual(hitungJumlah,1)
    
    def test_ngambilData(self):
        buatMenu.objects.create(Nama_Menu = "Kopi Susu",
        Harga_Menu = "Rp20.000",
        Masukkan_Foto_Menu = "https://assets-pergikuliner.com/Shm3HsZZN8e1BLORfHq3n2jnTP4=/385x290/smart/https://assets-pergikuliner.com/uploads/image/picture/1151477/picture-1542254875.jpg",
        id_toko=1)

        hitungJumlah = buatMenu.objects.all().count()
        self.assertEqual(hitungJumlah,1)

    # def test_gavalid(self):

    #     buatMenu.objects.create(Nama_Menu = "",
    #     Harga_Menu = "",
    #     Masukkan_Foto_Menu = ""
    #     )

    #     hitungJumlah = buatMenu.objects.all().count()
    #     self.assertEqual(hitungJumlah,0)

