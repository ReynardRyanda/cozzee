from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import buatMenu
from .forms import MenuForm
from landingpage.models import TambahToko

# Create your views here.
def TampilanMenu(request, id):
    semua_menu = buatMenu.objects.filter(id_toko = id)
    context = {"menu" : semua_menu,
    'id':id,
    'toko' : TambahToko.objects.get(id = id),
    }
    return render (request, 'TampilanMenuPage.html', context)

def TambahMenu(request, id):
    if request.method == "GET":      #Tampilin form-nya
        form = MenuForm()
        sent_data = {"menu_form" : form,
        'id':id
        }
        # return HttpResponse("ga masuk")
        return render (request, 'TambahMenuPage.html', sent_data)
    else: 
        isi_form = MenuForm(data=request.POST)     #dict yg isinya data yg telah di submit 
        if isi_form.is_valid():
            new_nama_menu = request.POST["Nama_Menu"]
            new_harga_menu = request.POST["Harga_Menu"]
            new_foto_menu = request.POST["Masukkan_Foto_Menu"]
            
            new_menu = buatMenu(
                Nama_Menu = new_nama_menu,
                Harga_Menu = new_harga_menu,
                Masukkan_Foto_Menu = new_foto_menu,
                id_toko = id
            )
            new_menu.save()
            return TampilanMenu(request, id)
        else:
            return HttpResponse("NOT SAVED")

# def DetailMenu(request, menu_id):
#     nama_object = buatMenu.objects.get(id = menu_id)
#     isi_context = {"isinya" : nama_object}
#     return render (request, 'MenuDetail.html', isi_context)

# def HapusMenu(request):
#     hapus = buatMenu.objects.all()
#     for x in hapus:
#         x.delete()
#     all = buatMenu.objects.all()
#     arg = {
#         "menu" : all
#     }
#     return render(request, 'TampilanMenuPage.html', arg)


    
            